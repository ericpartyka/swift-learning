//
//  SACustomTableViewCell.swift
//  Swifty App
//
//  Created by Eric Partyka on 12/7/16.
//  Copyright © 2016 Real Savvy Inc. All rights reserved.
//

import UIKit

class SACustomTableViewCell: UITableViewCell {

    @IBOutlet var theImageView: UIImageView!
    
    @IBOutlet var theTitleLabel: UILabel!
    
    @IBOutlet var theDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
