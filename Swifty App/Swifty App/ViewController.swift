///Users/ericpartyka/Desktop/Swifty App/Swifty App
//  ViewController.swift
//  Swifty App
//
//  Created by Eric Partyka on 12/1/16.
//  Copyright © 2016 Real Savvy Inc. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireImage

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var theTableView: UITableView!
    
    let theDataArray = [] as NSMutableArray

    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        self.configureNavBar()
        self.configureView()
        self.configureXIBs()
        self.configureDataSource()
    
        
    }
    
    // MARK: Memory Management

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Private Methods
    
    func configureNavBar (){
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = NSLocalizedString("Swifty Homes", comment: "nav bar title")
        
    }
    
    func configureView (){
        
    }
    
    func configureXIBs () {
        
        self.theTableView.register(UINib(nibName: "SACustomTableViewCell", bundle: nil), forCellReuseIdentifier: "SACustomTableViewCell")
        
    }
    
    func configureDataSource (){
        
        let theURLString = "https://www.realsavvy.com/api/v1/properties/10671655"
        self.didRetreiveData(theURL: theURLString)
        
    }

    // MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.theDataArray.count > 0
        {
            
            return self.theDataArray.count
        }
        else
        {
            return 0
        }
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SACustomTableViewCell", for: indexPath) as! SACustomTableViewCell
        
        //let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "UITableViewCell")
        self.configureCellAtIndexPath(cell: cell, theIndexPath: indexPath)
        
        return cell

    }
    

    func configureCellAtIndexPath(cell: SACustomTableViewCell, theIndexPath: IndexPath) -> Void
    {
        
        let theDict = self.theDataArray.object(at: theIndexPath.row) as! NSDictionary
        let thePropertyDict = theDict.object(forKey: "property") as! NSDictionary
        
        let theCityString = thePropertyDict.object(forKey: "city") as! String
        let theStateString = thePropertyDict.object(forKey: "state_code") as! String
        let theLocationString = String(format: "%@, %@", theCityString, theStateString)
        
        let theTitleString = thePropertyDict.object(forKey: "title")
        let thePriceValueString = String(format: "%@", thePropertyDict.object(forKey: "price_in_dollars") as! CVarArg)
        
        let theInfoString = String(format: "%@ - %@", theTitleString as! CVarArg, thePriceValueString)
        
        
        let thePhotosArray = [] as NSMutableArray
        
        thePhotosArray.add(thePropertyDict.object(forKey: "photo_urls")!)
        
        let theObjectArray = thePhotosArray.firstObject as AnyObject! as! NSArray
        
        let theURLString = theObjectArray.firstObject as! String
        let thePhotoURL = URL(string: theURLString)
        cell.theImageView.af_setImage(withURL: thePhotoURL!)
        
        print(theURLString)
        
        cell.theTitleLabel?.text = theLocationString
        cell.theDetailLabel.text = theInfoString
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailController = storyboard.instantiateViewController(withIdentifier: "SADetailController") as! SADetailController
        detailController.thePropertyArray = self.theDataArray
        
        self.navigationController?.pushViewController(detailController, animated: true)
        
    }
    
    
    func didRetreiveData (theURL: String)
    {
    
        SANetworkStore.didGetWithParams(theStringURLParam: theURL)
        
        Alamofire.request(theURL).responseJSON { response in
           
            if let JSON = response.result.value {
                
                self.theDataArray.add(JSON)
                self.theTableView.reloadData()
                
            }
        }
    
    }

}

