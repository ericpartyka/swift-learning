//
//  SANetworkStore.swift
//  Swifty App
//
//  Created by Eric Partyka on 12/8/16.
//  Copyright © 2016 Real Savvy Inc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class SANetworkStore: NSObject {
    
    
    class func didGetWithParams (theStringURLParam: String)  {
        
        Alamofire.request(theStringURLParam).responseJSON { response in
            
            if let JSON = response.result.value {
                
                print(JSON)
                
            }
        }
    }
}

