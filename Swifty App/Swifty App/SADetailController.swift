//
//  SADetailController.swift
//  Swifty App
//
//  Created by Eric Partyka on 12/7/16.
//  Copyright © 2016 Real Savvy Inc. All rights reserved.
//

import UIKit

class SADetailController: UIViewController {

    public var thePropertyArray = [] as NSMutableArray
    
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.configureNavBar()
    }
    
    // MARK: Private Methods
    
    func configureNavBar (){
        
        let theItemDict = self.thePropertyArray.firstObject as! NSDictionary
        let thePropertyObject = theItemDict.object(forKey: "property") as! NSDictionary
        
        let theTitleString = thePropertyObject.object(forKey: "title") as! NSString
        self.navigationItem.title = theTitleString as String
        
    }
    
    // MARK: Memory Managment

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Actions

    @IBAction func didPrssBackButton(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }

}
